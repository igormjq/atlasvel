FROM php:7.3-fpm

# Seta o contexto de build e acesso do container no diretório /var/www
WORKDIR /var/www

RUN rm -rf html/

# Reflete o UID do usuário da máquina com www-data
RUN usermod -u 1000 www-data

# Altera o dono da pasta do projeto para www-data
RUN chown -R www-data:www-data /var/www

# Seta permissões a moda bicho para pasta do projeto
RUN chmod 755 /var/www

# Helper incluido na imagem para lidar com instalação e habilitação de dependendências
RUN docker-php-ext-install pdo pdo_mysql

# Instala composer
RUN ( curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer )

RUN apt-get update \
    && apt-get install -y git

# Copia o arquivo de entrypoint da máquina para diretório '/' do container 
COPY ./.docker/entrypoint.sh /

# Torna a porta 9000 acessível a outros containeres
EXPOSE 9000

# Executa o arquivo entrypoint.sh 
ENTRYPOINT /entrypoint.sh