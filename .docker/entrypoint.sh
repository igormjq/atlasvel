#!/bin/bash
echo "Generating .env file"
cp .env.example .env

echo "Installing dependencies"
composer install

echo "Generating app key"
php artisan key:generate

# echo "Running migrations..."
# php artisan migrate

echo "Starting PHP FPM"
php-fpm